import React, { useReducer, createContext } from "react";

import storeReducer from './StoreReducer';
import { initialStore } from '../helpers/initialStore';

const StoreContext = createContext()

const StoreProvider = ({children}) => {

    const [store,dispatch] = useReducer(storeReducer, initialStore)

    return (
        <StoreContext.Provider value= {[store,dispatch]}>
            {children}
        </StoreContext.Provider>
    )
}

export default StoreProvider
export {StoreContext}