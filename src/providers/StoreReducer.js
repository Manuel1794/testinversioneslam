import { types } from '../helpers/types';

const storeReducer = (state,action) => {
    switch(action.type) {
        case types.createUser:
            return [...state, action.payload]
        case types.updateUser:{
            const todoEdit = action.payload
            return state.map(todo=>todo.id === todoEdit.id? todoEdit:todo)
        }
        case types.delete:
            return state.filter(filter=>filter.id !== action.payload)
        default:
            return state
    }
}

export default storeReducer