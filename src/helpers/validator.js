const regexEmail = /^(?:[^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*|"[^\n"]+")@(?:[^<>()[\].,;:\s@"]+\.)+[^<>()[\]\.,;:\s@"]{2,63}$/i

export const emailValidator = (user)=>{

    const {nombres ,apellidos,ciudad,username,correo} = user

    if(nombres.trim()===""||apellidos.trim()===""||correo.trim()===""||username.trim()===""||ciudad.trim()===""){
            return 'debes llenar todos los campos'
        }

    const validateEmail = (email) => {
        const patron = regexEmail
        return email.match(patron)
    }

    if(!validateEmail(user.correo)){
        return 'el email debe tener un formato válido'
    }

    if(nombres.length<5) {
        return "el nombre debe tener al menos 5 caracteres"
    }
    if(nombres.length>20) return "el nombre no debe ser mayor de 20 caracteres"

    if(correo.length<5) return "el correo debe tener al menos 10 caracteres"
    if(correo.length>30) return "el correo no debe ser mayor de 20 caracteres"

    if(apellidos.length<5) return "los apellidos deben tener al menos 5 caracteres"
    if(apellidos.length>20) return "los apellidos no deben ser mayor de 25 caracteres"

    if(username.length<5) return "el username debe tener al menos 5 caracteres"
    if(username.length>20) return "el username no debe ser mayor de 20 caracteres"

    if(ciudad.length<5) return "el nombre de la ciudad debe tener al menos 5 caracteres"
    if(ciudad.length>20) return "el nombre de la ciudad no debe ser mayor de 20 caracteres"
    
    return 'success'
}
