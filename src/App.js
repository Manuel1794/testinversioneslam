import React , {useState} from 'react'
import {AppBar, Box, Container, makeStyles, Typography} from '@material-ui/core'
import CustomerDialog from './components/CustomerDialog';
import AddUser from './components/AddUser';
import TableUsers from './components/TableUsers';
import StoreProvider from './providers/StoreProvider';
import DeleteUsuario from './components/DeleteUsuario';

const App = ()=>{
  
  const classes=useStyles()
  const [openForm, setOpenForm] = useState(false)
  const [valuesToEdit,setValuesToEdit] = useState(null)
  const [userToDelete,setUserToDelete] = useState(null)

  const editarUsuario = (user) => {
    setOpenForm(true)
    setValuesToEdit(user)
  }

  const handleDelete = (userId) => {
    console.log(userId)
    setUserToDelete(userId)
  }

  return (
    <Box className={classes.mainBg}>
      <Container>
        <AppBar position="static" className={classes.mainBar}>
          <Typography variant="h5" align="center" className={classes.mainTitle}>
              users list @inversioneslam.ar
          </Typography>
        </AppBar>
        <AddUser setOpenForm={setOpenForm}/>
        <StoreProvider>
          <TableUsers editUser={editarUsuario} handleDelete={handleDelete}/>
          <CustomerDialog 
            openForm={openForm} 
            setOpenForm={setOpenForm} 
            valuesToEdit={valuesToEdit}
            setValuesToEdit={setValuesToEdit}
          /> 
          <DeleteUsuario userToDelete={userToDelete} setUserToDelete={setUserToDelete}/>
        </StoreProvider>
      </Container>
    </Box>
    
  );
}

const useStyles = makeStyles((theme) =>({
  mainBg:{
    backgroundColor:'#fff',
    height:'100vh',
    marginTop:'0',
  },
  mainBar:{
    marginTop:'0.5rem',
    borderRadius:'15px',
    padding:'0.25rem',
    backgroundColor:'#205d76'
  },
  mainTitle:{
    color:'black'
  }
}))


export default App;