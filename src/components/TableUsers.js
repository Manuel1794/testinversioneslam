import React, {useContext} from 'react';

import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';

import { StoreContext } from '../providers/StoreProvider';
import { types } from '../helpers/types';

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: '#82cacd',
    color: '#205d76',
    fontSize:'1rem',
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
  tablaContainer:{
    marginTop:'2rem'
  },
  rowUser:{
    cursor:'pointer'
  },
  editIcon:{
    marginRight:'1rem'
  }
});

export default function TableUsers({editUser,handleDelete}) {

  const [store,dispatch] = useContext(StoreContext)

  const classes = useStyles();

  return (
    <TableContainer component={Paper} className={classes.tablaContainer}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell>Alias</StyledTableCell>
            <StyledTableCell align="right">Nombres</StyledTableCell>
            <StyledTableCell align="right">Apellidos</StyledTableCell>
            <StyledTableCell align="right">Ciudad</StyledTableCell>
            <StyledTableCell align="right">Correo</StyledTableCell>
            <StyledTableCell align="right">Acciones</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {store.map((row) => (
            <StyledTableRow key={row.id} className={classes.rowUser}>
              <StyledTableCell component="th" scope="row">
                {row.username}
              </StyledTableCell>
              <StyledTableCell align="right">{row.nombres}</StyledTableCell>
              <StyledTableCell align="right">{row.apellidos}</StyledTableCell>
              <StyledTableCell align="right">{row.ciudad}</StyledTableCell>
              <StyledTableCell align="right">{row.correo}</StyledTableCell>
              <StyledTableCell align="right">
                  <EditIcon className={classes.editIcon} onClick={()=>editUser(row)}/>
                  <DeleteIcon onClick={()=>handleDelete(row.id)}/>
              </StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
