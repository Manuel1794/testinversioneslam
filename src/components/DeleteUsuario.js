import React, { useContext, useState } from 'react'

import Dialog from '@material-ui/core/Dialog';
import { DialogContent, DialogTitle, makeStyles } from '@material-ui/core';
import Button from '@material-ui/core/Button';

import { StoreContext } from '../providers/StoreProvider';
import { types } from '../helpers/types';

const DeleteUsuario = ({userToDelete,setUserToDelete}) => {

    const classes = useStyles()

    const [store,dispatch] = useContext(StoreContext)

    const handleDelete = () => {
        dispatch({type:types.delete,payload:userToDelete})
        setUserToDelete(null)
    }

    const handleClose = () => {
        setUserToDelete(null)
    }

    return (
        <>
            <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={userToDelete}
            fullWidth={true} maxWidth='md' >
                <DialogTitle className={classes.dialogAll}>¿Deseas eliminar este usuario?</DialogTitle>
                <DialogContent className={classes.dialogAll}>
                    <Button className={classes.redButton} variant="outlined" color="secondary"
                        onClick={()=>handleDelete()}
                    >Eliminar</Button>
                    <Button variant="outlined" onClick={()=>handleClose()}>Cancelar</Button>
                </DialogContent>
                
            </Dialog>
        </>
    )
}

const useStyles = makeStyles(()=>({
    dialogAll:{
        textAlign:'center'
    },
    redButton:{
        marginRight:'1rem'
    }
}))

export default DeleteUsuario