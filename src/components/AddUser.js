import React from 'react'
import {IconButton, Typography} from '@material-ui/core'
import PersonAddIcon from '@material-ui/icons/PersonAdd';

const AddUser = ({setOpenForm}) => {
    return (
        <div style={{display:'flex',alignItems:'center', marginBottom:'-1rem',marginTop:'1rem'}}>
            <IconButton onClick={()=>setOpenForm(true)} style={{cursor:'pointer'}}>
                <PersonAddIcon />
            </IconButton>
            <Typography>
                Agregar Usuario
            </Typography>
        </div>
    )
}

export default AddUser
