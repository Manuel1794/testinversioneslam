import React, { useContext, useEffect, useState } from 'react';

import {Dialog, DialogActions, DialogContent, DialogTitle, 
        Button, Grid} from '@material-ui/core';
import {ValidatorForm, TextValidator} from 'react-material-ui-form-validator';

import { StoreContext } from '../providers/StoreProvider';
import { types } from '../helpers/types';
import { emailValidator } from '../helpers/validator';


const initialData={
    nombres:'',
    apellidos:'',
    ciudad:'',
    username:'',
    correo:''
}

const CustomerDialog = (props) => {

    const [store,dispatch] = useContext(StoreContext)

    const [dataUser, setDataUser] = useState(initialData)
    const {nombres ,apellidos,ciudad,username,correo} = dataUser
    const {setValuesToEdit, setOpenForm,valuesToEdit,openForm} = props

    const closeModal=()=>{
        setValuesToEdit(null)
        setDataUser(initialData)
        setOpenForm(false)
    }
    
    const handleChange = (e) => {
        setDataUser({...dataUser, [e.target.name]:e.target.value})
    }


    const handleSubmit = () => {

        if(emailValidator(dataUser) !== 'success'){
            return alert(emailValidator(dataUser))
        }
        dispatch({type:valuesToEdit?types.updateUser: types.createUser,payload:dataUser})
        setDataUser(initialData)
        setOpenForm(false)
        setValuesToEdit(null)
    }
    
    const handleClose = () => {
        setDataUser(initialData)
        setOpenForm(false)
        setValuesToEdit(null)
    }

    useEffect(()=>{
        if(valuesToEdit){
            setDataUser(valuesToEdit)
        }
    },[valuesToEdit])

    return (
        <Dialog
        fullWidth={true}
        maxWidth='lg'
        open={openForm}
        onClose={closeModal}
        aria-labelledby="max-width-dialog-title"
        >
            <DialogTitle>{valuesToEdit ?  'Actualizar' : 'Agregar'}  Usuario</DialogTitle>
            <ValidatorForm >
                <DialogContent>
                    <Grid container spacing={3}>
                        <Grid item xs={6}>
                            <TextValidator
                            variant="outlined"
                            margin="normal"
                            fullWidth
                            label="Nombres"
                            onChange={handleChange}
                            name="nombres"
                            value={nombres}
                            validators={['required']}
                            errorMessages={['Este campo es requerido']}
                            autoComplete='off'
                        />
                        </Grid>
                        <Grid item xs={6}>
                            <TextValidator
                            variant="outlined"
                            margin="normal"
                            fullWidth
                            label="Apellidos"
                            onChange={handleChange}
                            name="apellidos"
                            value={apellidos}
                            validators={['required']}
                            errorMessages={['Este campo es requerido']}
                            autoComplete='off'
                        />
                        </Grid>
                        <Grid item xs={6}>
                            <TextValidator
                                variant="outlined"
                                margin="normal"
                                fullWidth
                                label="Correo Electrónico"
                                onChange={handleChange}
                                name="correo"
                                value={correo}
                                validators={['required','isEmail']}
                                errorMessages={['Este campo es requerido','El correo debe tener un formato válido']}
                                autoComplete='off'
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextValidator
                                variant="outlined"
                                margin="normal"
                                fullWidth
                                label="Username"
                                onChange={handleChange}
                                name="username"
                                value={username}
                                validators={['required']}
                                errorMessages={['Este campo es requerido']}
                                autoComplete='off'
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextValidator
                                variant="outlined"
                                margin="normal"
                                fullWidth
                                label="Ciudad"
                                onChange={handleChange}
                                name="ciudad"
                                value={ciudad}
                                validators={['required']}
                                errorMessages={['Este campo es requerido']}
                                autoComplete='off'
                            />
                        </Grid>
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <Button type="submit" color="secondary" onClick={()=> {
                        if(!valuesToEdit){
                            dataUser.id=Date.now()
                        }
                        handleSubmit()
                    }}>
                       {valuesToEdit ? 'Actualizar' : 'Agregar'}
                    </Button>
                    <Button color="primary" onClick={()=>handleClose()}>
                        Cerrar
                    </Button>
                </DialogActions>
            </ValidatorForm>
        </Dialog>
    );
}

export default CustomerDialog;